import sys
import images
import final

def rotate_right(image):
    return final.rotate_right(image)


def mirror(image):
    return final.mirror(image)


def blur(image):
    return final.blur(image)


def greyscale(image):
    return final.grayscale(image)


def change_colors(image,to_change,to_change_to) -> list:
  return final.change_colors(image,to_change,to_change_to)


def rotate_colors(image, increment):
  return final.rotate_colors(image, increment)


def shift(image, dx, dy):
  return final.shift(image, dx, dy)



image_file = sys.argv[1]
image = images.read_img(image_file)

i = 2
while i < len(sys.argv):
    transform = sys.argv[i]

    if transform == "rotate_right":
        image = rotate_right(image)

    elif transform == "mirror":
        image = mirror(image)

    elif transform == "blur":
        image = blur(image)

    elif transform == "greyscale":
        image = greyscale(image)

    elif transform == "change_colors":
        r1 = int(sys.argv[3])
        g1 = int(sys.argv[4])
        b1 = int(sys.argv[5])
        r2 = int(sys.argv[6])
        g2 = int(sys.argv[7])
        b2 = int(sys.argv[8])
        to_change = (r1, g1, b1)
        to_change_to = (r2, g2, b2)
        image = change_colors(image, to_change, to_change_to)

    elif transform == "rotate_colors":
        increment = int(sys.argv[3])
        image = rotate_colors(image, increment)

    elif transform == "shift":
        dx = int(sys.argv[3])
        dy = int(sys.argv[4])
        image = shift(image, dx, dy)

    else:
        print("Transform not recognized")
        break

    i += 1

base_filename = image_file.split(".")[0]
output_file = base_filename + "_trans." + image_file.split(".")[1]

images.write_img(image, output_file)

print("Saved transformed image to", output_file)
