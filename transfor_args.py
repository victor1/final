
import final
import sys
import images


def change_colors(image,to_change,to_change_to) -> list:
  return final.change_colors(image,to_change,to_change_to)


def rotate_colors(image, increment):
  return final.rotate_colors(image, increment)


def shift(image, dx, dy):
  return final.shift(image, dx, dy)


def main():
  image_file = sys.argv[1]
  transform = sys.argv[2]

  image = images.read_img(image_file)

  if transform == "change_colors":
    r1 = int(sys.argv[3])
    g1 = int(sys.argv[4])
    b1 = int(sys.argv[5])
    r2 = int(sys.argv[6])
    g2 = int(sys.argv[7])
    b2 = int(sys.argv[8])
    to_change = (r1, g1, b1)
    to_change_to = (r2, g2, b2)
    image = change_colors(image, to_change,to_change_to)

  elif transform == "rotate_colors":
    increment = int(sys.argv[3])
    image = rotate_colors(image, increment)

  elif transform == "shift":
    dx = int(sys.argv[3])
    dy = int(sys.argv[4])
    image = shift(image, dx, dy)
  else:
    print("Transform not recognized")

  base_filename = image_file.split(".")[0]
  output_file = base_filename + "_trans." + image_file.split(".")[1]

  images.write_img(image, output_file)

  print("Saved transformed image to", output_file)

if __name__ == '__main__':
    main()