import images
import math

def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple, to_change_to: tuple) -> list[list[tuple[int, int, int]]]:
    (ancho, largo) = images.size(image)
    new_image = images.create_blank(ancho, largo)
    for x in range(ancho):
        for y in range(largo):
            if image[x][y] == to_change:
                new_image[x][y] = to_change_to
            else:
                new_image[x][y] = image[x][y]
    images.write_img(new_image, "cafe_changed_colors.jpg")
    return images.read_img("cafe_changed_colors.jpg")


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_img = images.create_blank(ancho, largo)
    for x in range(ancho):
        for y in range(largo):
            new_img[x][y] = image[y][x]
    final = mirror(new_img)
    images.write_img(final, "cafe_rotate_right.jpg")
    return images.read_img("cafe_rotate_right.jpg")


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_image = images.create_blank(largo, ancho)
    for x in range(largo):
        for y in range(ancho):
            new_image[x][y] = image[largo - x - 1][y]
    images.write_img(new_image, "cafe_mirror.jpg")
    return images.read_img("cafe_mirror.jpg")


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_image = images.create_blank(largo, ancho)
    for x in range(largo):
        for y in range(ancho):
            sum1 = (image[x][y][0] + increment) % 255
            sum2 = (image[x][y][1] + increment) % 255
            sum3 = (image[x][y][2] + increment) % 255
            new_image[x][y] = (sum1, sum2, sum3)
    images.write_img(new_image, "cafe_rotate_colors.jpg")
    return images.read_img("cafe_rotate_colors.jpg")


def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    blurred_image = []
    for i in range(len(image)):
        blurred_image.append([])
        for j in range(len(image[0])):
            avg_r, avg_g, avg_b = 0, 0, 0
            count = 0
            if i > 0:
                avg_r += image[i - 1][j][0]
                avg_g += image[i - 1][j][1]
                avg_b += image[i - 1][j][2]
                count += 1
            if i < len(image) - 1:
                avg_r += image[i + 1][j][0]
                avg_g += image[i + 1][j][1]
                avg_b += image[i + 1][j][2]
                count += 1
            if j > 0:
                avg_r += image[i][j - 1][0]
                avg_g += image[i][j - 1][1]
                avg_b += image[i][j - 1][2]
                count += 1
            if j < len(image[0]) - 1:
                avg_r += image[i][j + 1][0]
                avg_g += image[i][j + 1][1]
                avg_b += image[i][j + 1][2]
                count += 1

            avg_r = int(avg_r / count)
            avg_g = int(avg_g / count)
            avg_b = int(avg_b / count)

            blurred_image[i].append((avg_r, avg_g, avg_b))

    images.write_img(blurred_image, "cafe_blur.png")
    return blurred_image


def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_img = images.create_blank(largo, ancho)
    for x in range(largo):
        for y in range(ancho):
            if x+horizontal >= largo and y+vertical >= ancho:
                new_img[x][y] = image[x+horizontal-largo][y+vertical-ancho]
            elif x+horizontal < 0 and y+vertical < 0:
                new_img[x][y] = image[x + horizontal + largo][y + vertical + ancho]
            elif x+horizontal < 0 and y+vertical >= ancho:
                new_img[x][y] = image[x + horizontal + largo][y + vertical - ancho]
            elif x + horizontal >= largo and y + vertical < 0:
                new_img[x][y] = image[x + horizontal - largo][y + vertical + ancho]
            elif x+horizontal >= largo:
                new_img[x][y] = image[x + horizontal-largo][y+vertical]
            elif y+vertical >= ancho:
                new_img[x][y] = image[x + horizontal][y + vertical-ancho]
            elif x+horizontal < 0:
                new_img[x][y] = image[x + horizontal+largo][y + vertical]
            elif y+vertical < 0:
                new_img[x][y] = image[x + horizontal][y + vertical+ancho]
            else:
                new_img[x][y] = image[x+horizontal][y+vertical]
    images.write_img(new_img,"cafe_shift.jpg")
    return new_img


def crop(image: list[list[tuple[int, int, int]]], cx: int, cy: int, horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    new_img = images.create_blank(cx+horizontal, cy+vertical)
    (largo, ancho) = images.size(new_img)
    for x in range(largo):
        for y in range(ancho):
            new_img[x][y] = image[cx+x][cy+y]
    images.write_img(new_img,"cafe_crop.jpg")
    return new_img

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_img = images.create_blank(largo, ancho)
    for x in range(largo):
        for y in range(ancho):
            media = sum(image[x][y])//3
            new_img[x][y] = (media,media,media)
    images.write_img(new_img,"cafe_greyscale.jpg")
    return new_img


def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_img = images.create_blank(largo, ancho)
    for x in range(largo):
        for y in range(ancho):
            rojo = (image[x][y][0] * r)
            verde = (image[x][y][1] * g)
            azul = (image[x][y][2] * b)
            new_img[x][y] = (int(rojo),int(verde),int(azul))
    images.write_img(new_img,"cafe_filter.jpg")
    return new_img















#OPCIONAL














def sepia(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_img = images.create_blank(largo, ancho)
    for x in range(largo):
        for y in range(ancho):
            Sepiarojo = (0.39 * image[x][y][0]) % 255 + (0.77 * image[x][y][1]) % 255 + (0.19 * image[x][y][2]) % 255
            Sepiaverde = (0.35 * image[x][y][0]) % 255 + (0.69 * image[x][y][1]) % 255 + (0.17 * image[x][y][2]) % 255
            Sepiaazul = (0.27 * image[x][y][0]) % 255 + (0.53 * image[x][y][1]) % 255 + (0.13 * image[x][y][2]) % 255
            new_img[x][y] = (int(Sepiarojo), int(Sepiaverde), int(Sepiaazul))
    images.write_img(new_img,"cafe_sepia.jpg")
    return new_img


def negativo(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_img = images.create_blank(largo, ancho)
    for x in range(largo):
        for y in range(ancho):
            new_img[x][y] = (255-image[x][y][0], 255-image[x][y][1], 255-image[x][y][2])
    images.write_img(new_img,"cafe_negativo.jpg")
    return new_img


def corte_circulo(image: list[list[tuple[int, int, int]]], center_x: int, center_y: int, radius: int) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_img = images.create_blank(largo, ancho)
    for x in range(largo):
        for y in range(ancho):
            distance = math.sqrt((x - center_x) ** 2 + (y - center_y) ** 2)
            if distance <= radius:
                new_img[x][y] = image[x][y]
    images.write_img(new_img,"cafe_corte_circulo.jpg")
    return new_img


def contraste(image: list[list[tuple[int, int, int]]], factor: float) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_img = images.create_blank(largo, ancho)
    for x in range(largo):
        for y in range(ancho):
            r = int(image[x][y][0] * factor)
            g = int(image[x][y][1] * factor)
            b = int(image[x][y][2] * factor)
            new_img[x][y] = (r, g, b)
    images.write_img(new_img,"cafe_contraste.jpg")
    return new_img


def posterizar(image: list[list[tuple[int, int, int]]], niveles: int) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_img = images.create_blank(largo, ancho)
    for x in range(largo):
        for y in range(ancho):
            divisor = 255//niveles
            r = image[x][y][0] // divisor * divisor
            g = image[x][y][1] // divisor * divisor
            b = image[x][y][2] // divisor * divisor
            new_img[x][y]=(r,g,b)
    images.write_img(new_img,"cafe_posterizar.jpg")
    return new_img

def imagen_divididas_con_filtros_distintos(image: list[list[tuple[int, int, int]]], filtro1:callable, filtro2:callable,filtro3:callable,filtro4:callable) -> list[list[tuple[int, int, int]]]:
    (largo, ancho) = images.size(image)
    new_img = images.create_blank(largo, ancho)
    mid_width = largo // 2
    mid_height = ancho // 2

    top_left = [row[:mid_width] for row in image[:mid_height]]
    top_right = [row[mid_width:] for row in image[:mid_height]]
    bottom_left = [row[:mid_width] for row in image[mid_height:]]
    bottom_right = [row[mid_width:] for row in image[mid_height:]]
    top_left1 = filtro1(top_left)
    top_right2 = filtro2(top_right)
    bottom_left3 = filtro3(bottom_left)
    bottom_right4 = filtro4(bottom_right)
    for x in range(largo):
        for y in range(ancho):
            if x < mid_height and y < mid_width:
                new_img[x][y] = top_left1[x][y]
            elif x < mid_height and y >= mid_width:
                new_img[x][y] = top_right2[x][y-mid_width]
            elif x >= mid_height and y < mid_width:
                new_img[x][y] = bottom_left3[x-mid_height][y]
            else:
                new_img[x][y] = bottom_right4[x-mid_height][y-mid_width]
    images.write_img(new_img,"imagen_divididas_con_filtros_distintos.jpg")
    return new_img




def main():
    while True:
        print("escoje un filtro para tu imagen\n1: change color\n2: rotate_right\n3: mirror\n4: rotate color\n5: blur\n6:shift\n7: grayscale\n8: filter\n9: sepia\n10: negativo\n11: corte circulo\n12: contraste\n13: posterizar\n14: imagen dividida con filtros\n15: salir")
        x = input("escoje un filtro:")
        if x == "1":
            change_colors(images.read_img("cafe.jpg"),(0,0,0),(255,255,255))
        elif x == "2":
            rotate_right(images.read_img("cafe.jpg"))
        elif x == "3":
            mirror(images.read_img("cafe.jpg"))
        elif x == "4":
            rotate_colors(images.read_img("cafe.jpg"),12)
        elif x == "5":
            blur(images.read_img("cafe.jpg"))
        elif x == "6":
            shift(images.read_img("cafe.jpg"), 200, 200)
        elif x == "7":
            grayscale(images.read_img("cafe.jpg"))
        elif x == "8":
            filter(images.read_img("cafe.jpg"),3,6,2)
        elif x == "9":
            sepia(images.read_img("cafe.jpg"))
        elif x == "10":
            negativo(images.read_img("cafe.jpg"))
        elif x == "11":
            corte_circulo(images.read_img("cafe.jpg"), 100, 100, 50)
        elif x == "12":
            contraste(images.read_img("cafe.jpg"), 2.0)
        elif x == "13":
            posterizar(images.read_img("cafe.jpg"), 4)
        elif x == "14":
            imagen_divididas_con_filtros_distintos(images.read_img("cafe.jpg"), blur, sepia, negativo, grayscale)
        elif x == "15":
            break

#ad
if __name__ == '__main__':
    main()