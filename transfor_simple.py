import images
import final
import sys


def rotate_right(image):
  return final.rotate_right(image)


def mirror(image):
  return final.mirror(image)


def blur(image):
  return final.blur(image)


def greyscale(image):
  return final.grayscale(image)


def main():
  image_file = sys.argv[1]
  transform = sys.argv[2]
  image = images.read_img(image_file)
  if transform == "rotate_right":
    image = rotate_right(image)
  elif transform == "mirror":
    image = mirror(image)
  elif transform == "blur":
    image = blur(image)
  elif transform == "greyscale":
    image = greyscale(image)
  else:
    print("Transform not recognized")
  base_filename = image_file.split(".")[0]
  output_file = base_filename + "_trans." + image_file.split(".")[1]
  images.write_img(image, output_file)
  print("Saved transformed image to", output_file)

if __name__ == '__main__':
    main()